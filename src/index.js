const API_URL = "http://localhost:3000";
const keyCart = "cartItems";
const keyProducts = "cartProductsDetails";

const buttons = document.querySelectorAll(".store__category-button");
const productsList = document.querySelector(".store__list");
const cartButton = document.querySelector(".store__cart-button");
const cartCount = cartButton.querySelector(".store__cart-count");

const modalOverlay = document.querySelector(".modal-overlay");
const cartItemsList = document.querySelector(".modal__cart-items");
const modalCloseButton = document.querySelector(".modal-overlay__close-button");
const cartTotalPriceElement = document.querySelector(".modal__cart-price");
const cartForm = document.querySelector(".modal__cart-form");

const orderMessageElement = document.createElement("div");
orderMessageElement.classList.add("order-message");
const orderMessageText = document.createElement("p");
orderMessageText.classList.add("order-message__text");
const orderMessageButton = document.createElement("button");
orderMessageButton.classList.add("order-message__close-button");
orderMessageButton.textContent = "Закрыть";

orderMessageElement.append(orderMessageText, orderMessageButton);
orderMessageButton.addEventListener("click", () => {
    orderMessageElement.remove();
});

buttons.forEach(b => {
    b.addEventListener("click", changeCategory);
    if (b.classList.contains("store__category-button_active")) {
        fetchProducts(b.textContent);
    }
});

cartButton.addEventListener("click", async () => {
    modalOverlay.style.display = "flex";

    const cartItems = JSON.parse(localStorage.getItem(keyCart) || "[]");
    const ids = cartItems.map(item => item.id);

    if (!ids.length) {
        const listItem = document.createElement("li");
        cartItemsList.textContent = "";
        listItem.textContent = "Корзина пуста";
        cartItemsList.append(listItem);
        return;
    }

    const products = await fetchCartItems(ids);
    localStorage.setItem(keyProducts, JSON.stringify(products));

    renderCartItems();
});

modalOverlay.addEventListener("click", ({ target }) => {
    if (target === modalOverlay || target.closest("modal-overlay__close-button")) {
        modalOverlay.style.display = "none";
    }
});

productsList.addEventListener("click", ({ target }) => {
    if (target.closest(".product__btn-add-cart")) {
        const productId = target.dataset.id;
        addToCart(productId);
    }
});

cartItemsList.addEventListener("click", ({ target }) => {
    if (target.classList.contains("modal__plus")) {
        const productId = target.dataset.id;
        updateCartItem(productId, 1);
    }
    if (target.classList.contains("modal__minus")) {
        const productId = target.dataset.id;
        updateCartItem(productId, -1);
    }
});

cartForm.addEventListener("submit", submitOrder);

updateCartCount();

function changeCategory({ target }) {
    buttons.forEach(b => b.classList.remove("store__category-button_active"));
    target.classList.add("store__category-button_active");
    fetchProducts(target.textContent);
}

async function fetchProducts(category) {
    try {
        const response = await fetch(`${API_URL}/api/products/category/${category}`);
        if (!response.ok) {
            throw new Error(response.status);
        }

        const products = await response.json();
        renderProducts(products);
    } catch (error) {
        console.error(`Error fetch: ${error}`);
    }
}

async function fetchCartItems(ids) {
    try {
        const response = await fetch(`${API_URL}/api/products/list/${ids.join(",")}`);
        if (!response.ok) {
            throw new Error(response.status);
        }

        return await response.json();
    } catch (error) {
        console.error(`Error fetch: ${error}`);
        return [];
    }
}

function renderProducts(products) {
    productsList.textContent = "";
    products.forEach(p => {
        const productCard = createProductCard(p);
        productsList.append(productCard);
    });
}

function createProductCard({ id, photoUrl, name, price }) {
    const productCard = document.createElement("li");
    productCard.classList.add("store__item");
    productCard.innerHTML = `
        <article class="store__product product">
            <img src="${API_URL}${photoUrl}" class="product__image" alt="${name}" width="388" height="261">
            <h3 class="product__title">${name}</h3>
            <p class="product__price">${price}&nbsp;P</p>
            <button class="product__btn-add-cart" data-id="${id}">Заказать</button>
        </article>
    `;

    return productCard;
}

function addToCart(productId) {
    const cartItems = JSON.parse(localStorage.getItem(keyCart) || "[]");

    const existingItem = cartItems.find(item => item.id === productId);
    if (existingItem) {
        existingItem.count += 1;
    } else {
        cartItems.push({ id: productId, count: 1 });
    }

    localStorage.setItem(keyCart, JSON.stringify(cartItems));
    updateCartCount();
}

function updateCartCount() {
    const cartItems = JSON.parse(localStorage.getItem(keyCart) || "[]");
    cartCount.textContent = cartItems.length;
}

async function renderCartItems() {

    cartItemsList.textContent = "";

    const cartItems = JSON.parse(localStorage.getItem(keyCart) || "[]");
    const products = JSON.parse(localStorage.getItem(keyProducts) || "[]");

    products.forEach(({ id, photoUrl, name, price }) => {
        const cartItem = cartItems.find(item => item.id === id);
        if (!cartItem) {
            return;
        }

        const listItem = document.createElement("li");
        listItem.classList.add("modal__cart-item");
        listItem.innerHTML = `
            <img src="${API_URL}${photoUrl}" alt="${name}" class="modal__cart-item-image">
            <h3 class="modal__cart-item-title">${name}</h3>

            <div class="modal__cart-item-count">
                <button class="modal__btn modal__minus" data-id=${id}>-</button>
                <span class="modal__count">${cartItem.count}</span>
                <button class="modal__btn modal__plus" data-id=${id}>+</button>
            </div>

            <p class="modal__cart-item-price">${price * cartItem.count}&nbsp;Р</p>
        `;

        cartItemsList.append(listItem);
    });

    const totalPrice = calculateTotalPrice(cartItems, products);
    cartTotalPriceElement.innerHTML = `${totalPrice}&nbsp;Р`;
}

function calculateTotalPrice(cartItems, products) {
    return cartItems.reduce((acc, item) => {
        const product = products.find(prod => prod.id === item.id);
        return acc + product.price * item.count;
    }, 0);
}

function updateCartItem(productId, change) {
    const cartItems = JSON.parse(localStorage.getItem(keyCart) || "[]");
    const itemIndex = cartItems.findIndex(item => item.id === productId);

    if (itemIndex !== -1) {
        cartItems[itemIndex].count += change;

        if (cartItems[itemIndex].count <= 0) {
            cartItems.splice(itemIndex, 1); // Remove element from array
        }
        localStorage.setItem(keyCart, JSON.stringify(cartItems));

        updateCartCount();
        renderCartItems();
    }
}

async function submitOrder(event) {
    event.preventDefault();

    const storeId = cartForm.store.value;
    const cartItems = JSON.parse(localStorage.getItem(keyCart) || "[]");

    const products = cartItems.map(({ id, count }) => ({
        id,
        quantity: count
    }));

    try {
        const response = await fetch(`${API_URL}/api/orders`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ storeId, products })
        });

        if (!response.ok) {
            throw new Error(response.status);
        }

        localStorage.removeItem(keyCart);
        localStorage.removeItem(keyProducts);
        updateCartCount();

        const { orderId } = await response.json();
        orderMessageText.textContent = `Заказ: ${orderId}`;
        document.body.append(orderMessageElement)

        modalOverlay.style.display = "none";
    } catch (error) {
        console.error(`Order error: ${error}`);
    }
}